package main

import (
	"context"

	"github.com/mongodb/mongo-go-driver/bson/primitive"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"

	pb "gitlab.com/lazybasterds/alpaca/advertisement-service/proto/advertisement"
)

const (
	dbName               = "alpacadb"
	promotionsCollection = "promotions"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	GetActiveAdvertisements(ctx context.Context) ([]*pb.Advertisement, error)
	Close(ctx context.Context)
}

//AdvertisementRepository concrete implementation of the Repository interface.
type AdvertisementRepository struct {
	db *DBConnection
}

// GetActiveAdvertisements gets the information about the active advertisements
func (repo *AdvertisementRepository) GetActiveAdvertisements(ctx context.Context) ([]*pb.Advertisement, error) {
	collection := repo.collection(promotionsCollection)

	cur, err := collection.Find(ctx, bson.M{"active": true})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var ads []Advertisement

	for cur.Next(ctx) {
		var ad Advertisement
		err := cur.Decode(&ad)
		if err != nil {
			return nil, err
		}

		ads = append(ads, ad)

	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	var results = make([]*pb.Advertisement, 0, len(ads))

	//Convert model to proto object
	for _, a := range ads {
		results = append(results, &pb.Advertisement{
			Id:          a.ID.Hex(),
			Nodeid:      a.NodeID.Hex(),
			Title:       a.Title,
			Description: a.Description,
			Image:       toString(a.ImageList),
		})
	}

	return results, nil
}

//Close closes the session
func (repo *AdvertisementRepository) Close(ctx context.Context) {
	// TODO: Revisit if this is the correct context when closing session
	repo.db.session.EndSession(ctx)
}

func (repo *AdvertisementRepository) collection(coll string) *mongo.Collection {
	return repo.db.client.Database(dbName).Collection(coll)
}

func toString(objs []primitive.ObjectID) []string {
	result := make([]string, 0, len(objs))
	for _, obj := range objs {
		result = append(result, obj.Hex())
	}
	return result
}
