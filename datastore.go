package main

import (
	"context"
	"log"

	"github.com/mongodb/mongo-go-driver/mongo"
)

//DBConnection contains the different components of the MongoDB database connection.
type DBConnection struct {
	client   *mongo.Client
	database *mongo.Database
	session  mongo.Session
}

// CreateDBConnection creates a MongoDB connection to the database with session.
func CreateDBConnection(ctx context.Context, host string) (*DBConnection, error) {
	//Log here
	client, err := mongo.NewClient(host)
	if err != nil {
		return nil, err
	}
	// ctx, cancelFunc := context.WithTimeout(context.Background(), 10*time.Second)
	// defer cancelFunc()

	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	session, err := client.StartSession()
	if err != nil {
		return nil, err
	}

	// Log here
	return &DBConnection{
		client:  client,
		session: session,
	}, nil
}

//Close close the whole database session
func (db *DBConnection) Close(ctx context.Context) {
	db.session.EndSession(ctx)
	err := db.client.Disconnect(ctx)
	if err != nil {
		log.Println("WARNING: Some connections were still up when Disconnect was called, but it was closed regardless.")
	}
}
