package main

import (
	"context"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/advertisement-service/proto/advertisement"
)

type service struct {
	db *DBConnection
}

func (s *service) GetRepo() Repository {
	return &AdvertisementRepository{s.db}
}

func (s *service) GetActiveAdvertisements(ctx context.Context, req *pb.Advertisement, res *pb.AdvertisementResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	advertisements, err := repo.GetActiveAdvertisements(ctx)
	if err != nil {
		log.Printf("Error: Failed to get advertisements : %+v", err)
		return err
	}
	log.Printf("Method: GetActiveAdvertisements, RequestID: %s, Return: %+v", requestID, advertisements)
	res.Advertisements = advertisements
	return nil
}
