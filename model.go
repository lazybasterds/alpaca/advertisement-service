package main

import (
	"github.com/mongodb/mongo-go-driver/bson/primitive"
)

//Advertisement struct contains data for the promotion/ads
type Advertisement struct {
	ID          primitive.ObjectID   `bson:"_id,omitempty" json:"id,omitempty"`
	NodeID      primitive.ObjectID   `bson:"nodeid" json:"nodeid"`
	Title       string               `bson:"title" json:"title"`
	Description string               `bson:"description" json:"description"`
	Active      bool                 `bson:"active" json:"active"`
	ImageList   []primitive.ObjectID `bson:"image" json:"image"`
}
